var express = require('express')

var userRoutes = require('./routes/user.route')

var app = express()
var port = 3000

app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded
app.set('view engine','pug')
app.set('views','./views')
app.use(express.static('public'))


app.get('/',function(req,res){
    res.render('index',{
        name: 'Manh'
    })
})

app.use('/users',userRoutes)

app.listen(3000, function(){
    console.log("server running port : " ,port)
})